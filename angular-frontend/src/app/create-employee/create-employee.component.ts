import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import {NgToastService} from 'ng-angular-popup';
import { NotificationService } from '../notification.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  listForm=new FormGroup({
    email: new FormControl('',[Validators.required,Validators.email,
      Validators.pattern("[A-Za-z0-9\\-\\_\\.]+[@]+[a-z]+[\\.]+[A-Za-z]{2,3}")])
  })

  
  employee: Employee = new Employee();

  public employeeError!: Employee;
  private isSaved:boolean=false;
  private employeeExist:boolean=false;


  constructor(private employeeService: EmployeeService,private notifyService : NotificationService,
    private router: Router, private toast: NgToastService) { }

  ngOnInit(): void {
  }

  saveEmployee(){
    this.employeeService.createEmployee(this.employee).subscribe(data=>{
      console.log(data);
      this.gotoEmployeeList();
      this.employee=new Employee();
      this.isSaved = true;
      this.employeeExist = false;
      this.employeeError = new Employee();
      this.showToasterSuccess();

    },
    error=> {
      this.employeeError = error.error;
      this.isSaved = false;
      if(error.status==406){
        this.isSaved = false;
        this.employeeExist = true;

        if(this.employeeError.name){
          this.showToasterErrorname();
        }
        if(this.employeeError.email){
          this.showToasterErroremail();
        }
        if(this.employeeError.age){
          this.showToasterErrorage();
        }

      }
      
      console.log(error)

    })

  }

  gotoEmployeeList(){
    this.router.navigate(['/employees']);
  }

  onSubmit(){
    console.log(this.employee);
    this.saveEmployee();
  }

  showToasterSuccess(){
    this.notifyService.showSuccess("Employee created successfully !!", "");
}

showToasterErrorname(){
  this.notifyService.showWarning("Plz enter correct name", "")
}
showToasterErroremail(){
  this.notifyService.showWarning("Plz enter valid Email", "")
}
showToasterErrorage(){
  this.notifyService.showWarning("Age must be between 18-99", "")
}

}
