import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  id!: number;
  employee: Employee=new Employee();

  public employeeError!: Employee;
  private isSaved:boolean=false;
  private employeeExist:boolean=false;

  constructor(private employeeService: EmployeeService, private route: ActivatedRoute,
    private router: Router,private notifyService : NotificationService) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.employeeService.getEmployeeById(this.id).subscribe(data=>{
      this.employee=data;
    },error=> console.log(error));
  }

  onSubmit(){
    this.employeeService.updateEmployee(this.id, this.employee).subscribe(data=>{
      this.gotoEmployeeList();
      this.showToasterInfo();
    },error=> console.log(error));

    
  }

  gotoEmployeeList(){
    this.router.navigate(['/employees']);
  }


  showToasterInfo(){
    this.notifyService.showInfo("Updated successfully", "")
}
}
